import 'dart:convert';
import 'dart:io';

import 'package:butler/src/Butler.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import '../Butler.dart';

class ButlerWorker
{
  var _path="";
  var _paramaters = Map<String, dynamic>();
  var type="";

  var _token = "";
  var _ip = "";
  var _port = "";
  var _fullURL = "";

  ButlerWorker.POST()
  {
    type = "POST";
  }
  ButlerWorker.GET()
  {
    type = "GET";
  }
  ButlerWorker.DELETE()
  {
    type = "DELETE";
  }
  ButlerWorker.PUT()
  {
    type = "PUT";
  }
  ButlerWorker.FORM()
  {
    type = "FORM";
  }

  ButlerWorker Initialize(ip,port)
  {
    _ip = ip;
    _port = port;
    _fullURL= "http://"+_ip+":"+_port;
    return this;
  }
  ButlerWorker Path(path)
  {
    _path = path;
    return this;
  }
  ButlerWorker Paramater(key,value)
  {

    _paramaters[key] = value;
    return this;
  }
  ButlerWorker Paramaters(paramaters)
  {

    _paramaters = paramaters;
    return this;
  }
  ButlerWorker SetToken(token)
  {
    _token= token;
  }
  void Then(callback)
  {
    if(type == "post")
    {
      _Post(callback);
    }
    else  if(type == "get")
    {
      _Get(callback);
    }
    else if(type == "put")
    {
      _Put(callback);
    }
    else  if(type == "delete")
    {
      _Delete(callback);
    }
    else  if(type == "form")
    {
      _Form(callback);
    }

  }
  dynamic ThenAsync(callback)
  {
    if(type == "post")
    {
      return _Post(null);
    }
    else  if(type == "get")
    {
      return _Get(null);
    }
    else if(type == "put")
    {
      return _Put(null);
    }
    else  if(type == "delete")
    {
      return _Delete(null);
    }
    else  if(type == "form")
    {
      return _Form(null);
    }

  }




  Future<dynamic> _Post(callback)
  async {


    final JsonDecoder _decoder = new JsonDecoder();
    var response;
    print(_fullURL+"/"+_path);
    if(_token != null)
    {

      response = await http.post(_fullURL+"/"+_path, body: _paramaters,headers: {HttpHeaders.authorizationHeader: _token});

    }
    else
    {
      response = await http.post(_fullURL+"/"+_path, body: _paramaters);

    }


    var decoder = _decoder.convert(response.body);

    if(callback == null)
      {
        return decoder;
      }
    else
      {
        callback(decoder);
      }
    return null;

  }
  Future<dynamic> _Get(callback)
  async {

    final JsonDecoder _decoder = new JsonDecoder();
    print(_fullURL+"/"+_path);
    var response;
    if(_token != null)
    {
      response= await http.get(_fullURL+"/"+_path,headers: {HttpHeaders.authorizationHeader: _token});
    }
    else
    {
      response= await http.get(_fullURL+"/"+_path,);
    }

    var decoder = _decoder.convert(response.body);
    if(callback == null)
    {
      return decoder;
    }
    else
    {
      callback(decoder);
    }
    return null;
  }
  Future<dynamic> _Delete(callback)
  async {
    final JsonDecoder _decoder = new JsonDecoder();
    var response;
    if(_token != null)
    {
      response = await http.delete(_fullURL+"/"+_path,headers: {HttpHeaders.authorizationHeader: _token});
    }
    else
    {
      response = await http.delete(_fullURL+"/"+_path);
    }

    var decoder = _decoder.convert(response.body);
    if(callback == null)
    {
      return decoder;
    }
    else
    {
      callback(decoder);
    }
    return null;
  }
  Future<dynamic> _Put(callback)
  async {
    final JsonDecoder _decoder = new JsonDecoder();
    var response;
    if(_token != null) {
      response = await http.put(_fullURL + "/" + _path, body: _paramaters,headers: {HttpHeaders.authorizationHeader: _token});
    }
    else{
      response = await http.put(_fullURL + "/" + _path, body: _paramaters);
    }
    var decoder = _decoder.convert(response.body);
    if(callback == null)
    {
      return decoder;
    }
    else
    {
      callback(decoder);
    }
    return null;
  }

  void _Form(callback)
  {
    FormData data = FormData();

    _paramaters.forEach((k,v) async {



      if(v is File)
      {
        File a = v;

        final mimeType = lookupMimeType(a.path);

        String fileName = v.path.split('/').last;
        data.files.add(MapEntry(k,  await MultipartFile.fromFile(
            v.path,
            filename: fileName,
            contentType:new MediaType('image', 'jpeg')
        )));
      }
         else if(v is List<File>)
        {
          for(int i=0;i<v.length;i++)
            {
              //coklu resim
              print("Dosya işleniyor");
              String fileName = v[i].path.split('/').last;
              data.files.addAll([MapEntry(k,  await MultipartFile.fromFile(
                  v[i].path,
                  filename: fileName,
                  contentType:new MediaType('image', 'jpeg')
              )
            )].toList());
            }
        }
      else
      {
        data.fields.add(MapEntry(k,v));
      }
    });
    Dio dio = new Dio();

    dio.post(_fullURL+"/"+_path, data: data)
        .then(callback)
        .catchError((error) => print(error));
  }
}
